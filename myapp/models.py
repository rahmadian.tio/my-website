from django.db import models
from django.utils import timezone

class Jadwals(models.Model):
	nama = models.CharField(max_length = 30)
	tipe = models.CharField(max_length = 30)
	tanggal = models.DateField(max_length = 30, default = timezone.now)
	waktu = models.TimeField(max_length = 30, default = timezone.now)
	tempat = models.CharField(max_length = 30)
	
class Status(models.Model):
	status =models.CharField(max_length = 400)
	
	def __str__(self):
		return self.status

class regis(models.Model):
	name = models.CharField(max_length = 30)
	email = models.EmailField(max_length = 30)
	password= models.CharField(max_length = 30)
	def as_dict(self):
		return{
			"name": self.name,
			"email": self.email,
		}