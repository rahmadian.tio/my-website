from django import forms
from .models import Jadwals, Status, regis

class FormJadwal(forms.Form):
	attrsText = {
		'class' : 'form-control'
	}
	
	attrsDate = {
		'class' : 'form-control',
		'type' : 'date'
	}
	
	attrsTime = {
		'class' : 'form-control',
		'type' : 'time'
	}
	nama = forms.CharField(label = 'Aktivitas', max_length = 30, widget = forms.TextInput(attrs = attrsText), required = True)
	tipe = forms.CharField(label = 'Tipe', max_length = 30, widget = forms.TextInput(attrs = attrsText), required = True)
	tanggal = forms.DateField(label = 'Tanggal', widget = forms.TextInput(attrs = attrsDate), required = True)
	waktu = forms.TimeField(label = 'Waktu', widget = forms.TextInput(attrs = attrsTime), required = True)
	tempat = forms.CharField(label = 'Lokasi', max_length = 30, widget = forms.TextInput(attrs = attrsText), required = True)

class formStatus(forms.Form):
	status = forms.CharField(label ='status', required = True, max_length = 400, widget = forms.TextInput(attrs = {'class' : 'form-control'}))
	
class register(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super(register, self).__init__(*args, **kwargs)
		
		self.fields['name'].label = ''
		self.fields['name'].widget.attrs = {'class': 'form-control', 'id': 'nameInput', 'placeholder': 'Full Name'}
		
		self.fields['email'].label = ''
		self.fields['email'].widget.attrs = {'class': 'form-control', 'id': 'emailInput', 'placeholder': 'Email'}
		
		self.fields['password'].label = ''
		self.fields['password'].widget = forms.PasswordInput()
		self.fields['password'].widget.attrs = {'class': 'form-control', 'id': 'passInput', 'placeholder': 'Password'}
		
	class Meta:
		model = regis
		fields = ['name', 'email', 'password']