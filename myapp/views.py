from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from .forms import FormJadwal, formStatus, register
from .models import Jadwals, Status, regis
import requests, webbrowser, json

response = {'author' : 'Rahmadian Tio'}
def home (request):
	return render(request, 'Web.html')
	
def guestbook (request):
	return render(request, 'Form.html')
	
def profile(request):
	return render(request, 'myProfile.html')

def library(request):
	return render(request, 'library.html')

def datajson(request):
	nama = request.GET.get('cari', 'quilting')
	json_file = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + nama)
	response = json_file.json()
	return JsonResponse(response)
		
def Jadwal(request):
	jwl = Jadwals.objects.all()
	form = FormJadwal(request.POST)
	if request.method == 'POST':
		if 'DeleteAll' in request.POST :
			Jadwals.objects.all().delete()
			context = {
				'form' : form,
				'jwl' : jwl
			}
			return render(request, 'Jadwal.html', context)	
		response['nama'] = request.POST['nama']
		response['tipe'] = request.POST['tipe']
		response['tanggal'] = request.POST['tanggal']
		response['waktu'] = request.POST['waktu']
		response['tempat'] = request.POST['tempat']
		jdwl = Jadwals(nama = response['nama'], tipe = response['tipe'], tanggal = response['tanggal'], waktu = response['waktu'], tempat = response['tempat'])
		jdwl.save()
		form = FormJadwal()
		context = {
			'form' : form,
			'jwl' : jwl
		}
		return render(request, 'Jadwal.html', context)
	
	else:
		form = FormJadwal()
		context = {
			'form' : form,
			'jwl' : jwl
		}
		return render(request, 'Jadwal.html', context)
	
def landpage(request):
	statusRes = Status.objects.all()
	response['statusRes'] = statusRes
	response['form_status'] = formStatus
	return render(request, "landpage.html", response)
		
def post(request):
	form_status = formStatus(request.POST or None)
	if(request.method == 'POST' and form_status.is_valid()):
		response['status'] = request.POST['status']
		status = Status(status = response['status'])
		status.save()
		return HttpResponseRedirect('/')

	else:
		return HttpResponseRedirect('/')
		
def registrasi(request):
    form = register()
    context = {
        'form' : form,
    }
    return render(request, 'registrasi.html', context)
    
def cek(request):
    emails = request.POST['email']
    regs = {
        'salah': regis.objects.filter( email = emails).exists()
    }
    return JsonResponse(regs)
    
def submitRegis(request):
    form = register(request.POST)
	if form.is_valid():
		form.save()
		return JsonResponse({'hasil' : "True"})
	
	else:
		return JsonResponse({'hasil' : "False"})
		
def listRegis(request):
	pengikut = regis.objects.all()
	return render(request, 'listRegis.html')
	
