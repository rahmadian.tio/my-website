from django.conf.urls import url
from .views import home , guestbook, Jadwal, landpage, profile, datajson, library, registrasi, submitRegis, cek
from django.conf import settings
#url for append	
urlpatterns = [
	url('Web', home),
	url('profile', profile),
	url('Form', guestbook),
	url('Jadwal', Jadwal),
	url('landpage', landpage, name="post"),
	url('library', library, name='library'),
	url('datajson', datajson, name='datajson'),
	url('registrasi/cek', cek, name='cek'),
	url('registrasi', registrasi, name='registrasi'),
	url('submitRegis', submitRegis, name='submitRegis'),
	url('', home)
]