$(document).ready(function () {
    $.ajax({
        url : "/book_data",
        dataType : "json",
        success : function (datajson) {
            var book_data = datajson.items;
            for (var i = 0; i < book_data.length; i++) {
                var table_content = "<tr>" +
                    "<td>" + '<img src="'+ book_data[i].volumeInfo.imageLinks.smallThumbnail +'" class="img-fluid rounded mx-auto d-block" alt="'+book_data[i].volumeInfo.previewLinks+'">' + "</td>" +
                    "<th scope=\"row\">" + book_data[i].volumeInfo.title + "</th>" +
                    "<td>" + book_data[i].volumeInfo.authors + "</td>" +
                    "<td>" + book_data[i].volumeInfo.publishedDate + "</td>" +
                    "<td class=\"favorite star-symbol\"><a href='#'>☆</a></td>" +
                "</tr>";
                $('#table-body').append(table_content);
            };
        },
        
        error: function (error) {
            var table_content = "<tr><td colspan=\"5\" class = \"text-center\">JSON failed to loaded :(</td></tr>";
            $('#table-body').append(table_content);
        },
    });

    var count_favorite = 0;
    update_favorite();
    function update_favorite() {
        $('#favorite-count').text(count_favorite);
    };

    $(document).on('click', '.favorite', function(){
        if($(this).text() == "☆"){
            count_favorite++;
            update_favorite();
            $(this).text("★");
        }
        else{
            count_favorite--;
            update_favorite();
            $(this).text("☆");
        }
    });

    $( "#accordion" ).accordion({
        active: false,
        collapsible: true,
        heightStyle: "content"
    });
    
    var collapsible = $( "#accordion" ).accordion( "option", "collapsible" );
    $( "#accordion" ).accordion( "option", "collapsible", true );

    var heightStyle = $( "#accordion" ).accordion( "option", "heightStyle" );
    $( "#accordion" ).accordion( "option", "heightStyle", "content" );

});
