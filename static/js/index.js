$( function() {
    $( "#accordion" ).accordion({
        collapsible: true,
        active: false
    });
    var local = window.localStorage;
    if(local.getItem('isBlack') === 'true'){
        $("body").removeClass('bodi');
		$('.bodi body').removeClass('bodi');
		$('.bodi body').addClass('bodi-2');
	}
    else{
        $("body").addClass("bodi");
		$('.bodi body').addClass('bodi');
		$('.bodi body').removeClass('bodi-2');
    }
    document.getElementById('ubah-tema').onclick = Berubah;
    function Berubah() {
        if ($("body").hasClass('bodi')) {
            $("body").removeClass('bodi');
			$('.bodi body').removeClass('bodi');
			$('.bodi body').addClass('bodi-2');
			local.setItem('isBlack', true);
        }
        else {
            $("body").addClass("bodi");
			$('.bodi body').addClass('bodi');
			$('.bodi body').removeClass('bodi-2');
			local.setItem('isBlack', false);
        }
    }
});

$(document).ready(function() {
	$("#splash").delay(3000).fadeOut(500, function(){
	$(".bodi body").fadeIn(500);
	});
});
	